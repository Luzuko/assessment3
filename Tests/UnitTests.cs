﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections;
using System.Collections.Generic;
using Microservice;

namespace Tests
{
    [TestClass]
    public class UnitTests
    {
        
        [TestMethod]
        public void CheckIfDuplicatesFoundWhenPlacedAtStartOfArray()
        {
            List<String> duplist = new List<String>();
            List<String> expectedDuplist = new List<String>();

            for(int i = 5; i <= 15; i++)
            {
                duplist.Add(i.ToString());

                //duplicate five and six at the beginning of the list
                if (i == 5 || i == 6)
                {
                    duplist.Add(i.ToString());
                }
            }
          
            List<String> actualList = MicroserviceClass.ProcessList(duplist);
            
            expectedDuplist.Add("5"); expectedDuplist.Add("6");

            CollectionAssert.AreEqual(expectedDuplist, actualList);
        }

        [TestMethod]
        public void CheckDuplicateFoundWhenPlacedAtEndOfArray()
        {
            List<String> duplist = new List<String>();
            List<String> expectedDuplist = new List<String>();

            for (int i = 0; i <= 10; i++)
            {
                duplist.Add(i.ToString());

                //duplicate five and six at the beginning of the list
                if (i == 9 || i == 10)
                {
                    duplist.Add(i.ToString());
                }
            }

            List<String> actualList = MicroserviceClass.ProcessList(duplist);

            expectedDuplist.Add("9"); expectedDuplist.Add("10");

            CollectionAssert.AreEqual(expectedDuplist, actualList);
        }

        [TestMethod]
        public void CheckDuplicateFoundWhenDuplicateIsZero()
        {
            List<String> duplist = new List<String>();
            List<String> expectedDuplist = new List<String>();

            for (int i = 0; i <= 10; i++)
            {
                duplist.Add(i.ToString());

                //duplicate zero 
                if (i == 0)
                {
                    duplist.Add(i.ToString());
                }
            }

            List<String> actualList = MicroserviceClass.ProcessList(duplist);

            expectedDuplist.Add("0");

            CollectionAssert.AreEqual(expectedDuplist, actualList);
        }

        [TestMethod]
        public void CheckDuplicateFoundWhenDuplicateIsNegative()
        {
            List<String> duplist = new List<String>();
            List<String> expectedDuplist = new List<String>();

            for (int i = -9; i <= -1; i++)
            {
                duplist.Add(i.ToString());

                //duplicate five and six at the beginning of the list
                if (i == -9 || i == -8)
                {
                    duplist.Add(i.ToString());
                }
            }

            List<String> actualList = MicroserviceClass.ProcessList(duplist);

            expectedDuplist.Add("-9"); expectedDuplist.Add("-8");

            CollectionAssert.AreEqual(expectedDuplist, actualList);
        }

        [TestMethod]
        public void CheckDuplicateFoundWhenDuplicateIsPositive()
        {
            List<String> duplist = new List<String>();
            List<String> expectedDuplist = new List<String>();

            for (int i = 0; i <= 15; i++)
            {
                duplist.Add(i.ToString());

                //duplicate five and six at the beginning of the list
                if (i == 8 || i == 7 || i == 6 || i == 5)
                {
                    duplist.Add(i.ToString());
                }
            }

            List<String> actualList = MicroserviceClass.ProcessList(duplist);

            expectedDuplist.Add("5"); expectedDuplist.Add("6"); expectedDuplist.Add("7"); expectedDuplist.Add("8");

            CollectionAssert.AreEqual(expectedDuplist, actualList);
        }

    }
}
