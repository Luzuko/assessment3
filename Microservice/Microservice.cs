﻿using System;
using System.Collections;
using System.Collections.Generic;
using Microservice;

namespace Microservice
{
    public class MicroserviceClass
    {
        List<String> list = new List<String>();

        static void Main(string[] args)
        {
            List<String> list = new List<String>();

            // Form a list of numbers from 0-9.
            for (int i = 0; i < 10; i++)
            {
                list.Add(i.ToString());
            }
            // Insert a new set of numbers from 0-5.
            for (int i = 0; i < 5; i++)
            {
                list.Add(i.ToString());
            }

            Console.WriteLine("Input list : " + list);
            Console.WriteLine("\nFiltered duplicates : " + ProcessList(list));
        }

        public static List<String> ProcessList(List<String> listContainingDuplicates)
        {
            List<String> resultSet = new List<String>();
            HashSet<String> tempSet = new HashSet<String>();

            foreach (String yourInt in listContainingDuplicates)
            {
                if (!tempSet.Add(yourInt.ToString()))
                {
                    resultSet.Add(yourInt.ToString());
                }
            }
            return resultSet;
        }
    }
}
